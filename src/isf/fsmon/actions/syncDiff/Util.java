/*
 * Created on 03-mar-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package isf.fsmon.actions.syncDiff;

import isf.fsmon.Manager.Nodo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * @author UF371231
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Util {
	public static String getCompound(Nodo newFile){
		String rootTarget=(String)Inicializa.parametros.get("targetDir");
		String relFileName=newFile.getRelativePath();
		return rootTarget+"/"+relFileName;
	}
	public static void copy(Nodo newFile) throws Exception{
		String orgName=newFile.getCanonicalName();
		String destName=Util.getCompound(newFile);
		File destFile=new File(destName);
		if(destFile.exists())
			destFile.delete();
		File parentDest=destFile.getParentFile();
		if(!parentDest.exists())
			parentDest.mkdirs();
		FileInputStream fis=new FileInputStream(orgName);
		FileOutputStream fos=new FileOutputStream(destName);
		int c;
		while( (c=fis.read())!=-1 )
			fos.write(c);
		fos.close();
		fis.close();
	}
}
