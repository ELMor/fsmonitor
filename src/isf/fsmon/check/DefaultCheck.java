/*
 * Created on 01-feb-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package isf.fsmon.check;

import java.io.File;

import isf.fsmon.Check;

/**
 * @author UF371231
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class DefaultCheck implements Check {

	/* (non-Javadoc)
	 * @see isf.fsmon.Check#init(java.io.File)
	 */
	public Object init(File f) {
		if(f.exists()){
			return new FileAttrHolder(1,f.lastModified());
		}else{
			return new FileAttrHolder(0,0);
		}
	}

	public boolean isChanged(File f, Object oldCheck) {
		return !init(f).equals(oldCheck);
		
	}
}
