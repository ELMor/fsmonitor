/*
 * Created on 01-feb-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package isf.fsmon;


/**
 * @author UF371231
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public abstract class Action {
	public void print(Manager.Nodo newFile, String msg) throws Exception {
		System.out.println(
				(newFile.isdir ? "Directorio":"Archivo"  ) + 
				" "+ msg + " " + 
				newFile.name);

	}
	public abstract void perform(Manager.Nodo newFile) throws Exception;
}
